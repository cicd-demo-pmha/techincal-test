# techincal-test

# Solución al Reto Técnico Propuesto

## General

En esta solucion logramos resolver los requerimeintos propuestos por el reto, sin embargo el tener que alojar los servicios en tiers gratuitos de proveedores cloud supusieron restricciones para la eficiencia de la solución, particularmente para alojar la base de datos y para alojar una cola de mensajes como proposumos para una solución óptima.
### Endpoints

La dirección para consumir este servicio es:

https://products-exp-api-eq123o.5sc6y6-1.usa-e2.cloudhub.io

Las credenciales para consumir son:
 - client_id: 9b50567ecc53484d9bfaaf50b7e679b2
 - client_secret: 5F4aF9016f4947369754b5fB01BbB8e1
Estas credenciales debe ir en headers.

Se diseñaron varios endpoints para manejar diferentes funcionalidades de la API:

- POST `/category`: Endpoint para crear nuevas categorias en nuestra base de datos.
- GET `/product?page=integer&pageSize=integer`: Endpoint para obtener productos que estén en nuestra base de datos. Por defecto el método trae los 30 primeros registros que se encuentren en nuestra base de datos. Sin embargo, cunado se agregan los queryParams *page* y *pageSize* podemos traer registros paginados. *page* determina el offset y *pageSize* la cantidad de registros.
- GET `/product/{ID}`: Endpoint para obtener un producto particular presente en nuestra base de datos.
- POST `/product`: Endpoint para agregar registros a la base de datos 1 a 1.
- POST `/product-mass`: Endpoint para agregar registros de productos a la base de datos de manera asíncrona. Internamente, el flow publica a una queue de Kafka (idealmente sería AnypointMQ), y la api de proceso se encarga de consolidar 30 registros o los registros disponibles cada 3 segundos y hacer un bulk insert a la base de datos, reduciendo el estrés en los endpoints y en la base de datos.

Los esquemas de datos están definidos 
## Diseño y Arquitectura de la Solución

La solución se diseñó siguiendo una arquitectura de microservicios, particularmente el apiled-philosophy propuesto por mule, utilizando MuleSoft como plataforma de integración. También se quiso implementar (aunque hosteado localmente) un patrón de diseño basado en eventos usando Kafka para manejar la comunicación asíncrona entre capas de integración, ayudando a los endpoints y a la capacidad de computo a manejar mejor los spikes de requests propuestos. A continuación se muestra un diagrama de la arquitectura en detalle:

![Diagrama de Arquitectura](/images/arquitectura.png)

- **Capa de Presentación**: APIs REST expuestas a través de MuleSoft que gestionan las peticiones HTTP de los clientes. 0.5vCores
- **Capa de Proceso**: Lógica de negocio implementada en MuleSoft, donde se procesan y validan las solicitudes antes de interactuar con los servicios subyacentes. 0.2vCores
- **Capa de Sistema**: Integración con sistemas externos y manejo de eventos a través de Kafka para operaciones asíncronas como la actualización de inventario o la generación de notificaciones. 0.2vCores
- **Capa de Datos**: Persistencia de datos en una base de datos relacional Postgres para almacenar la información de productos y categorias, alojado en render.com.

Así mismo, la especificación de la api en RAML (recomendado por Mulesoft) se encuentra en `/API-SPEC/products-exp-api.zip`, o también está disponible para consultarla en el public portal en: 

https://anypoint.mulesoft.com/exchange/portals/holcim-330/a53a9f47-7909-4b6c-b6fd-008e1c2f89bb/products-api/

## Seguridad de la API

La seguridad de la API se implementó utilizando client credentials previamente autorizadas para la api de experiencia, idealmente deberia ser una autenticación con tokens, pero para este POC considero que es suficiente con estas credenciales.

Internamente, las apis tienen todos los datos sensibles cifrados y no quedan expuestos si se revisara su codigo, por medio de secure credentialas. El secreto para poder manejar estas configuraciones es: *technicaltest*.

## Implementación

### API Experiencia

La API se diseñó con una estructura RESTful, siguiendo buenas prácticas de diseño de APIs para mejorar la experiencia del desarrollador y facilitar la integración con sistemas externos. Esta capa gestiona la autorización para el consumo y también la redirección para el procesamiento de mensajes. Cuando se utiliza el endpoint /product-mass los mensajes se envían a una cola kafka y se procesan de forma asíncrona para favorecer los picos de peticiones, este endpoint solo se puede acceder si se hostea la cola de manera local, pues no encontre un hosting gratuito para este proposito, y AnypointMQ no está dispopnible en el frie tier de Mulesoft. 

Idealmente esta sería la solución, pero debido a las restricciones de tier/hosting libre no fue posible.

### API Proceso

Esta capa procesa los mensajes recibidos de la capa de experiencia y para el endpoint /product envía uno a uno cada producto individualmente a la capa del sistema y a la base de datos, lo que es costoso y supone alto estés para la base de datos con alto número de peticiones. 

Para el flujo masivo que se corre localmente, la api consume en mensajes desde la cola o todos los mensajes disponibles en un intervalo de tiempo para luego unir mensajes (productos) y enviar una consulta unificada a la base de datos, esto favorece los picos de peticiones, la resiliencia de la api y el estrés de la base de datos.

Como el endpoint/flujo asincrono por cola de mensajes no lo podemos probar en el endpoint proporcionado, mostrare el funcionamient y performance estimado de este enfoque que resulta mas eficiente que el enfoque ad-hoc que se encuentra en linea.

![KAFKA](/images/intialdb.png)

En la imagen anterior vemos la base de datos con una carga inicial.

![petitions](/images/testdb.png)

Utilizamos JMeter para poder generar multiples peticiones a ese endpoint y simular el flujo de peticion -> publicar en kafka -> consumir topic desde capa prc -> Insertar en bulk en la db.

Para la prueba tenemos 50 usuarios mandando peticiones concurrentes incrementandose cada 10 seguntos infinitamente (lo dejamos por 2 minutos).

![requests](/images/totalrequests.png)
![final](/images/finaldb.png)

Luego de recopilar los productos/mensajes en la capa de proceso y procesarlos para posteriormente enviarlos por lotes a la base de datos, podemos ver el resultado en la base de datos alojada en linea. Por ser un free tier de hosting para la base de datos hay un cierto deplay en los inserts y lo que podemos ver con un SELECT *, por eso la discrepancia entre los requests de Jmeter y los rows disponibles en la db. 

### API Sistema

Esta capa solo maneja interacciones con BD, recibe consultas de la capa de procesos y solo las ejecuta a través de su conector. Esto favorece la api-led y la responsabilidad única, haciendo esta api multicanal y disponible para otras integraciones. Si se utilizara para otros canales/integraciones se debería evaluar como se escalan los recursos si horizontal o verticalmente para soportar la nueva volumetría.

### Base de Datos

Se utilizó una base de datos relacional Postgres para almacenar la información de productos. Se optimizó el esquema de base de datos y se implementaron índices para mejorar el rendimiento de las consultas, como se especificó en el diagrama.

Ela archivo de inicialización de este esquema se encuentra en el directorio root con nombre `init.sql`.

### Kafka

Se configuró un clúster de Kafka para manejar la comunicación asíncrona entre los diferentes componentes del sistema. Se implement ún topic de Kafka 'products-topic' para consumir este tipo de evento (ej. creación de producto) y se implementó la lógica de consumidores y productores en MuleSoft.

Idealmente y para mantener todo en el mismo ecosistema, esto se deberia hacer en AnypointMQ pero para hacer este POC es suficiente.

