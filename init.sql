CREATE EXTENSION IF NOT EXISTS pgcrypto;

-----------------------------------
---------  CATEGORIES -------------
CREATE TABLE IF NOT EXISTS Categories (
    CategoryID INTEGER PRIMARY KEY NOT NULL,
    CategoryName VARCHAR(30) UNIQUE,
    Description VARCHAR(30),
    Picture VARCHAR(80)
);

-----------------------------------
---------  SUPPLIERS --------------
CREATE TABLE IF NOT EXISTS Suppliers (
    SupplierID INTEGER PRIMARY KEY NOT NULL,
    CompanyName VARCHAR(20),
    ContactTitle VARCHAR(20),
    Address VARCHAR(20),
    City VARCHAR(20),
    Region VARCHAR(20),
    PostalCode VARCHAR(20),
    Country VARCHAR(20),
    Phone VARCHAR(20),
    Fax VARCHAR(20),
    HomePage VARCHAR(20)
);
CREATE INDEX idx_Suppliers_CompanyName ON Suppliers(CompanyName);
CREATE INDEX idx_Suppliers_PostalCode ON Suppliers(PostalCode);

-----------------------------------
----------  SHIPPERS --------------
CREATE TABLE IF NOT EXISTS Shippers (
    ShipperID INTEGER PRIMARY KEY NOT NULL,
    CompanyName VARCHAR(20),
    Phone VARCHAR(20)
);

-----------------------------------
---------   CUSTOMERS -------------
CREATE TABLE IF NOT EXISTS Customers (
    CustomerID INTEGER PRIMARY KEY NOT NULL,
    CompanyName VARCHAR(20),
    ContactName VARCHAR(20),
    ContactTitle VARCHAR(20),
    Address VARCHAR(20),
    City VARCHAR(20),
    Region VARCHAR(20),
    PostalCode VARCHAR(20),
    Country VARCHAR(20),
    Phone VARCHAR(20),
    Fax VARCHAR(20)
    ---- CONSTRAINTS
);
CREATE INDEX idx_Customers_City ON Customers(City);
CREATE INDEX idx_Customers_CompanyName ON Customers(CompanyName);
CREATE INDEX idx_Customers_PostalCode ON Customers(PostalCode);
CREATE INDEX idx_Customers_Region ON Customers(Region);

-----------------------------------
---------   EMPLOYEES -------------
CREATE TABLE IF NOT EXISTS Employees (
    EmployeeID INTEGER PRIMARY KEY NOT NULL,
    LastName VARCHAR(20),
    FirstName VARCHAR(20),
    Title VARCHAR(20),
    TitleOfCourtesy VARCHAR(20),
    BirthDate VARCHAR(20),
    HireDate VARCHAR(20),
    Address VARCHAR(20),
    City VARCHAR(20),
    Region VARCHAR(20),
    PostalCode VARCHAR(20),
    Country VARCHAR(20),
    HomePhone VARCHAR(20),
    Extension VARCHAR(20),
    Photo VARCHAR(20),
    Notes VARCHAR(20),
    ReportsTo VARCHAR(20)
    ---- CONSTRAINTS
);
CREATE INDEX idx_Employees_LastName_FirstName ON Employees(LastName,FirstName);
CREATE INDEX idx_Employees_PostalCode ON Employees(PostalCode);

-----------------------------------
---------  PRODUCTS -----------
CREATE TABLE IF NOT EXISTS Products (
    ProductID INTEGER PRIMARY KEY NOT NULL,
    ProductName VARCHAR(20),
    SupplierID INTEGER,
    CategoryID INTEGER,
    QuantityPerUnit INTEGER,
    UnitPrice INTEGER,
    UnitsInStock INTEGER,
    UnitsOnOrder INTEGER,
    ReOrderLevel VARCHAR(20),
    Discontinued BOOLEAN,
    ---- CONSTRAINTS
    FOREIGN KEY (SupplierID) REFERENCES Suppliers(SupplierID),
    FOREIGN KEY (CategoryID) REFERENCES Categories(CategoryID)
);
CREATE INDEX idx_Products_ProductName ON Products(ProductName);
CREATE INDEX idx_Products_SupplierID ON Products(SupplierID);
CREATE INDEX idx_Products_CategoryID ON Products(CategoryID);

-----------------------------------
---------  ORDERS -----------
CREATE TABLE IF NOT EXISTS Orders (
    OrderID INTEGER PRIMARY KEY NOT NULL,
    CustomerID INTEGER,
    EmployeeID INTEGER,
    OrderDate DATE,
    RequiredDate DATE,
    ShippedDate DATE,
    ShipVia INTEGER,
    Freight VARCHAR(20),
    ShipName VARCHAR(20),
    ShipAddress VARCHAR(20),
    ShipCity VARCHAR(20),
    ShipRegion VARCHAR(20),
    ShipPostalCode VARCHAR(20),
    ShipCountry VARCHAR(20),
    ---- CONSTRAINTS
    FOREIGN KEY (CustomerID) REFERENCES Customers(CustomerID),
    FOREIGN KEY (EmployeeID) REFERENCES Employees(EmployeeID),
    FOREIGN KEY (ShipVia) REFERENCES Shippers(ShipperID)
);
CREATE INDEX idx_Orders_CustomerID ON Orders(CustomerID);
CREATE INDEX idx_Orders_EmployeeID ON Orders(EmployeeID);
CREATE INDEX idx_Orders_ShippedDate ON Orders(ShippedDate);
CREATE INDEX idx_Orders_ShipVia ON Orders(ShipVia);
CREATE INDEX idx_Orders_ShipPostalCode ON Orders(ShipPostalCode);

-----------------------------------
---------  ORDERDETAILS -----------
CREATE TABLE IF NOT EXISTS OrderDetails (
    OrderID INTEGER NOT NULL,
    ProductID INTEGER,
    UnitPrice INTEGER,
    Quantity INTEGER,
    Discount INTEGER,
    PRIMARY KEY (orderID, productID),
    ---- CONSTRAINTS
    FOREIGN KEY (OrderID) REFERENCES Orders(OrderID),
    FOREIGN KEY (ProductID) REFERENCES PRODUCTS(ProductID)
);
CREATE INDEX idx_OrderDetails_OrderID ON OrderDetails(OrderID);
CREATE INDEX idx_OrderDetails_ProductID ON OrderDetails(ProductID);